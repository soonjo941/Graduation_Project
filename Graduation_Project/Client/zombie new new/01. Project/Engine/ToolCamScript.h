#pragma once
#include "Script.h"

class CToolCamScript :
	public CScript
{
private:
	float		m_fSpeed;
	float		m_fScaleSpeed;
	//float		m_fZoomScaleY = 13;
	//float		m_fZoomScaleZ = 13;
	float		m_fZoomScaleY = 1.5;
	float		m_fZoomScaleZ = 1.5;
	float		m_fZoomSpeed = 0.005;
	bool		IsTargetDead = false;
	bool		IsNoTarget = false;
	CGameObject* m_pShootObject;
	CScene* pScene;

	Vec3 vPreviousPlayerPos = Vec3(0.f, 0.f, 0.f);
	Vec3 vPos = Vec3(0.f, 0.f, 0.f);
public:
	virtual void update();
	void SetShootObject(CGameObject* _pShootObject) { m_pShootObject = _pShootObject; }
	CGameObject* GetShootObject() { return m_pShootObject; };
	bool CameraTargetDead() { 	return IsTargetDead;};
	void SetTargetDead(bool b) { IsTargetDead = b; };
	void SetPreviousPlayerPos(Vec3 vPos) { vPreviousPlayerPos = vPos; };
	void SetCameraPos(Vec3 vPos) { vPos = vPos; };
	bool FindNewTarget();

public:
	CLONE(CToolCamScript);

public:
	CToolCamScript(CGameObject* _pPlayer, CScene* pscene);
	virtual ~CToolCamScript();
};
