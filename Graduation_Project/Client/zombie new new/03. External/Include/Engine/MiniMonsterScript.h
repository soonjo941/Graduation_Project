#pragma once
#include "Script.h"
#include "BehaviourTree.h"
#include "PlayerScript.h"
#include "MonsterScript.h"
#include "BossScript.h"
#include "BParticleSystem.h"

struct MiniMonsterStatus
{
	MonsterState state;
	float distanceToPlayer = 0;
	float attackRange = 100;
	float attackDamage = 30.f;
	float speed = 300.f;
	bool PlayerInRange = false;
	bool PlayerInAttackRange = false;
	bool isAttack = false;
	float recognizeRange = 1000.f;	// 인지 범위
	float hp = 50;
	float disappearCnt = 0;
	bool IsDisappear = false;
	bool IsCollide = false;
	bool BoomSoundPlay = false;
	CGameObject* TargetObject;

	// 특수 총알 효과 지속 시간
	float IceTime = 0.f;
	float FireTime = 0.f;
	float ThunderTime = 0.f;

	// 파티클
	bool IsFParticleOn = false;
	bool IsTParticleOn = false;
	bool IsIParticleOn = false;

	bool IsBoom = false;
	float boomTime = 1.0f;
	float FullboomTime = 1.0f;

	CGameObject* BParticleObject;
};

class CheckPlayerInRange2 : public Node {
private:

	MiniMonsterStatus* status;
	CGameObject* pObject;
	CScene* pScene;

public:

	CheckPlayerInRange2(MiniMonsterStatus* status, CGameObject* pObject, CScene* pscene) : status(status), pObject(pObject), pScene(pscene) {}
	virtual bool run() override {
		if (status->distanceToPlayer <= status->recognizeRange)
		{
			status->PlayerInRange = true;

			//상태 변경
			if (status->state != MonsterState::M_Run && status->distanceToPlayer > status->attackRange)
			{
				status->state = MonsterState::M_Run;

				//애니메이션 변경
				Ptr<CMeshData> pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\MiniZombieRun.mdat", L"MeshData\\MiniZombieRun.mdat");
				pObject->ChangeAnimation(pMeshData);
			}
		}
		else
		{
			status->PlayerInRange = false;

			if (status->state != MonsterState::M_Wander)
			{
				status->state = MonsterState::M_Wander;

				//애니메이션 변경
				Ptr<CMeshData> pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\MiniZombieRun.mdat", L"MeshData\\MiniZombieRun.mdat");
				pObject->ChangeAnimation(pMeshData);
			}
		}
		return status->PlayerInRange;
	}
};

class CheckPlayerInAttackRange2 : public Node {
private:
	MiniMonsterStatus* status;
	CGameObject* pObject;
	CScene* pScene;
public:
	CheckPlayerInAttackRange2(MiniMonsterStatus* status, CGameObject* pObject, CScene* pscene) : status(status), pObject(pObject), pScene(pscene) {}
	virtual bool run() override {
		if (status->distanceToPlayer <= status->attackRange)
		{
			status->PlayerInAttackRange = true;

			//상태변경
			if (status->state != MonsterState::M_Attack || status->hp <= 0)
			{
				status->state = MonsterState::M_Attack;

				for (int i = 0; i < MAX_LAYER; ++i)
				{
					const vector<CGameObject*>& vecObject = pScene->GetLayer(i)->GetObjects();
					for (size_t j = 0; j < vecObject.size(); ++j)
					{
						// 미니맵에 플레이어 위치 업데이트5
						if (L"Monster Object" == vecObject[j]->GetName() || L"Boss Object" == vecObject[j]->GetName() || L"Player Object" == vecObject[j]->GetName())
						{
							Vec3 monsterPos = vecObject[j]->Transform()->GetLocalPos();
							Vec3 Pos = pObject->Transform()->GetLocalPos();

							Vec3 sub = Pos - monsterPos;
							float length = sqrt(sub.x * sub.x + sub.y * sub.y + sub.z * sub.z);
							if (length < 150.f)
							{
								if (vecObject[j]->GetName() == L"Monster Object")
									vecObject[j]->GetScript<CMonsterScript>()->GetStatus()->hp -= status->attackDamage;
				
								else if(vecObject[j]->GetName() == L"Boss Object")
									vecObject[j]->GetScript<CBossScript>()->GetStatus()->hp -= status->attackDamage;

								else if(vecObject[j]->GetName() == L"Player Object" && !vecObject[j]->GetScript<CPlayerScript>()->GetStatus()->IsRoll)
									vecObject[j]->GetScript<CPlayerScript>()->getDamage(status->attackDamage);
							}
						}
					}
				}

				status->hp = 0;
				status->state = MonsterState::M_Die;
			}
		}

		return status->PlayerInAttackRange;
	}
};


class CMiniMonsterScript :
	public CScript
{
private:
	MiniMonsterStatus* status;

	Sequence* root;
	Sequence* sequence1;

	CheckPlayerInRange2* CCheckRange;
	CheckPlayerInAttackRange2* CCheckAttackRange;

	CGameObject* pObject;
	CScene* pScene;
	int targetNum;
	CGameObject* targetObjects[4];
	CGameObject* HpBarObject;
	CGameObject* FParticleObject;
	CGameObject* TParticleObject;
	CGameObject* IParticleObject;

	CSound* BoomSound;

public:

	virtual void update();

	virtual void OnCollisionEnter(CCollider2D* _pOther);
	virtual void OnCollisionExit(CCollider2D* _pOther);
	virtual void OnCollision(CCollider2D* _pOther);

	MiniMonsterStatus* GetStatus() { return status; };
	void SetStatus(MiniMonsterStatus* st);
	int findNearTarget();

	void checkParticle();

public:
	CLONE(CMiniMonsterScript);

public:
	CMiniMonsterScript(CGameObject* TargetObject[], int ntargetNum, CGameObject* Object, CScene* pscene);
	virtual ~CMiniMonsterScript();
};


