#pragma once

#include "Script.h"

class CRoarShotScript :
	public CScript
{
private:
	Vec3 vBulletStartPos;
	Vec3 vBulletDir;
	float BulletSpeed = 500.f;
	float damage = 30;
	bool BulletActive = false;
	float AddDamage = 0.f;

public:
	virtual void update();
	virtual void OnCollisionEnter(CCollider2D* _pOther);
	virtual void OnCollision(CCollider2D* _pOther);

public:
	CLONE(CRoarShotScript);

	float GetDamage() { return damage; };
	bool GetActive() { return BulletActive; };
	void SetActive(bool b) { BulletActive = b; };

	void SetDir(Vec3 d) { vBulletDir = d; };


	void SetStartPos(Vec3 pos) { vBulletStartPos = pos; };



public:
	CRoarShotScript();
	//CBulletScript();
	virtual ~CRoarShotScript();
};

