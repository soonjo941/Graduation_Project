#pragma once
#include "stdafx.h"

template<class T>
class CSingleton
{
protected:
	CSingleton() {};
	~CSingleton() {};

public:
	CSingleton(const CSingleton&);
	CSingleton& operator = (const CSingleton&);

	static T& GetInst()
	{
		static T instacne;
		return instacne;
	}
};

