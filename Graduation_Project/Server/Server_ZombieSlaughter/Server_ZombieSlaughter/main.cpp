#include "stdafx.h"

#include "PlayerObject.h"
#include "Zombie.h"

enum EOP_TYPE { OP_RECV, OP_SEND, OP_ACCEPT };
struct MY_OVER
{
	//send recv할때 마다 overlapped, 버퍼는 독립적이어야한다.
	WSAOVERLAPPED m_over;

	unsigned char m_buf[MAX_BUFFER];  //왜 unsigned이냐면 마이너스 값이 오지 않는다.
	EOP_TYPE m_eOP;//send인지 recv인지 구분한다.

	WSABUF m_wsabuf[1];
	SOCKET c_socket;

};
enum CL_STATE { PLST_FREE, PLST_CONNECTED, PLST_INGAE };
struct CLIENT
{
	mutex m_cl;// client lock
	MY_OVER m_recv_over;
	SOCKET m_socket;
	int m_id;
	unsigned int m_prev_size;

	//이부분 포인터로 해야할까 다른것으로 해야할까?
	CPlayerObject* m_pPlayer;
	CL_STATE m_state;
};

constexpr int SERVER_ID = 0;
array<CLIENT, MAX_USER + 1> clients;

std::array<CZombie, MAX_MONSTER> zombieArr;


mutex hl;//호스트 락
//atomic< bool> g_host_flag = false; //호스트가 없으면 false 호스트 있으면 true;
atomic<int> g_host_id = -1; //호스트 아이디 아무도 없으면 -1


int get_new_id(SOCKET c_sock)
{
	for (int i = SERVER_ID + 1; i <= MAX_USER; ++i)
	{
		lock_guard<mutex> lg{ clients[i].m_cl };
		if (clients[i].m_state == PLST_FREE) {
			clients[i].m_state = PLST_CONNECTED;
			clients[i].m_socket = c_sock;
			clients[i].m_pPlayer->init();
			return i;
		}
	}
	return -1;
}
int get_new_hostId()
{
	for (int i = SERVER_ID + 1; i <= MAX_USER; ++i)
	{
		if (i == g_host_id) continue;
		lock_guard<mutex> lg{ clients[i].m_cl };
		if (clients[i].m_state == PLST_CONNECTED) {
			if (clients[i].m_pPlayer->GetSceneState() == SCENE_STATE::GAME_SCENE)
				return i;

		}
	}
	return -1;
}


void roll_update(c2s_roll_start* packet);
void send_add_client(int c_id, int other_id);
void send_zombieInfo(int zombieID, int c_id);
void send_packet(int c_id, void* packet)
{

	int bufSize = reinterpret_cast<unsigned char*>(packet)[0];
	int bufType = reinterpret_cast<unsigned char*>(packet)[1];
#ifdef _DEBUG
	//std::cout << "send pacekt[" << bufType << "]" << " to [" << c_id << "]client\n";
#endif // _DEBUG

	MY_OVER* send_over = new MY_OVER;
	memset(&send_over->m_over, 0, sizeof(send_over->m_over));
	memcpy(send_over->m_buf, packet, bufSize);
	send_over->m_eOP = EOP_TYPE::OP_SEND;
	send_over->m_wsabuf[0].buf = reinterpret_cast<char*>(send_over->m_buf);
	send_over->m_wsabuf[0].len = bufSize;

	int retval = WSASend(clients[c_id].m_socket, send_over->m_wsabuf, 1, NULL, 0, &send_over->m_over, 0);
	if (retval != NO_ERROR)
	{
		int err_code = WSAGetLastError();
		if (err_code != WSA_IO_PENDING)
			err_display("WSASEND()", err_code);
	}
}
void send_move_packet(int c_id, int other_id) //c_id에게 other_id 정보 보내기
{
	CLIENT& client = clients[other_id];
	s2c_move packet;
	packet.size = sizeof(packet);
	packet.type = S2C_MOVE;
	packet.id = other_id;

	//좌표설정
	Vec3 pos = client.m_pPlayer->GetPostion();
	packet.x = pos.x; packet.y = pos.y; packet.z = pos.z;
	Vec3 rot = client.m_pPlayer->GetRotation();
	packet.rx = rot.x; packet.ry = rot.y; packet.rz = rot.z;

	//플레이어상태설정
	packet.ePlayerState = client.m_pPlayer->GetState();

	//s2c_dummy p;
	//p.size = sizeof(p);
	//p.type = S2C_DUMMY;
	send_packet(c_id, &packet);
}

void send_login_result(int c_id)
{
	s2c_loginOK packet;
	packet.size = sizeof(packet);
	packet.type = S2C_LOGIN_OK;
	packet.id = c_id;

	send_packet(c_id, &packet);
}

void send_change_scene(int c_id, SCENE_STATE Scenestate)
{
	s2c_change_Scene packet;
	packet.size = sizeof(packet);
	packet.type = S2C_CHANGE_SCENE;
	packet.eScene_state = Scenestate;
	packet.host_id = g_host_id;
	send_packet(c_id, &packet);
}

void upadate_scene_state(int c_id, c2s_chage_scene* packet)
{
	switch (packet->eSceneStatae)
	{
	case SCENE_STATE::START_SCENE:
	{
		//클라현재씬이 스타트씬에서 씬변환 패킷이 날라온경우
		//서버에서 관리하는 클라이언트 씬상태를 inGame상태로 바꿈
		if (-1 == g_host_id) {  //호스트가 -1이면
			g_host_id = c_id;
		}

		clients[c_id].m_cl.lock();
		clients[c_id].m_pPlayer->SetSceneState(SCENE_STATE::GAME_SCENE);

		send_change_scene(c_id, SCENE_STATE::GAME_SCENE); //처음에 씬


		for (auto& c : clients)//이미 플레이어늰 초기화 된데이터를 보냈다.
		{
			if (c_id == c.m_id) continue;
			lock_guard<mutex>{c.m_cl};
			cout << "c_id" << c_id << endl;
			if (clients[c.m_id].m_pPlayer->GetSceneState() == SCENE_STATE::GAME_SCENE) { //game씬상태인 클라이언트에게 addclient보내기
				send_add_client(c.m_id, c_id);//이미접속한 클라이언트들에게 새로접속한 클라이언트 정보보내기
				//if (c_id != c.m_id) //중복피하기위해서
				send_add_client(c_id, c.m_id);//새로접속한 클라이언트에게 이미접속한 클라이언트 정보보내기
			}
		}

		for (int i = 0; i < MAX_MONSTER; ++i) {  //이부분 계속 버그나서 일단 주석처리
			//몬스터 정보 보내기
			for (auto& c : clients)
			{
				//if (c_id == c.m_id) continue;
				//lock_guard<mutex>{c.m_cl};
				if (c.m_pPlayer->GetSceneState() == SCENE_STATE::GAME_SCENE) {
					//npc 이동했다고 알리기
					send_zombieInfo(i, c_id);  //c_id에게 i좀비 정보 보내기
				}
			}
		}

		clients[c_id].m_cl.unlock();

	}
	break;
	case SCENE_STATE::GAME_SCENE:
		break;
	case SCENE_STATE::GAMECLEAR_SCENE:
		break;
	case SCENE_STATE::GAMEOVER_SCENE:
		break;
	default:
		break;
	}



}
void send_key_result(int c_id, c2s_Key* packet)
{
	clients[c_id].m_pPlayer->Update(packet);

	for (auto& c : clients) {
		c.m_cl.lock();
		if (c.m_pPlayer->GetSceneState() == SCENE_STATE::GAME_SCENE) //게임씬일때 보내기
			send_move_packet(c.m_id, c_id); //c.secnod.m_id에게 c_id 정보 보내기
		c.m_cl.unlock();
	}
}
/// <summary>
/// c_id가 other_id를 추가한다.
/// </summary>
/// <param name="c_id"> 주체</param>
/// <param name="other_id"> 대상</param>
void send_add_client(int c_id, int other_id)
{
	const Vec3& otherRotation = clients[other_id].m_pPlayer->GetRotation();
	const Vec3& otherPos = clients[other_id].m_pPlayer->GetPostion();
	s2c_add_client packet;
	packet.size = sizeof(packet);
	packet.type = S2C_ADD_PLAYER;
	packet.id = other_id;
	packet.rx = otherRotation.x; packet.ry = otherRotation.y; packet.rz = otherRotation.z;
	packet.x = otherPos.x; packet.y = otherPos.y; packet.z = otherPos.z;

	packet.eScene_state = clients[other_id].m_pPlayer->GetSceneState();

	send_packet(c_id, &packet);
}

void send_roll_packet(int c_id, int other_id)
{
	const auto& dir = clients[other_id].m_pPlayer->GetRotation();
	s2c_roll_start packet;
	packet.size = sizeof(s2c_roll_start);
	packet.type = S2C_ROLL_START;
	packet.id = other_id;
	packet.x = dir.x;
	packet.y = dir.y;
	packet.z = dir.z;
	send_packet(c_id, &packet);

}
void send_roll_end_packet(int c_id, int other_id)
{
	const auto& pos = clients[other_id].m_pPlayer->GetRotation();
	s2c_roll_end packet;
	packet.size = sizeof(s2c_roll_end);
	packet.type = S2C_ROLL_END;
	packet.id = other_id;
	packet.px = pos.x;
	packet.py = pos.y;
	packet.pz = pos.z;
	send_packet(c_id, &packet);
}
void roll_update(c2s_roll_start* packet)
{
	int c_id = packet->id;

	clients[c_id].m_pPlayer->UpdateRollStart(packet);
	for (auto& c : clients) {
		lock_guard<mutex>{c.m_cl};
		if (c.m_pPlayer->GetSceneState() == SCENE_STATE::GAME_SCENE && c.m_id != c_id) //게임씬일때 보내기
			send_roll_packet(c.m_id, c_id); //c.secnod.m_id에게 c_id 정보 보내기
	}
}

void send_fire_bullet(int p_id, int c_id)
{
}

void proccess_packet(int c_id, unsigned char* buf)
{
	//buf[1]에 type이 들어감
	switch (buf[1])
	{
	case C2S_LOGIN: //클라에서 로그인요청
	{
		c2s_login* packet = reinterpret_cast<c2s_login*>(buf);
		send_login_result(c_id);
	}
	break;
	case C2S_KEY_EVENT:
	{
		c2s_Key* packet = reinterpret_cast<c2s_Key*>(buf);
		send_key_result(c_id, packet);
	}
	break;
	case C2S_CHANGE_SCENE: //클라에서 씬바꾸는 이벤트 일어남
	{
#ifdef _DEBUG
		cout << "c2sChange_sCene\n";
#endif // _DEBUG

		c2s_chage_scene* packet = reinterpret_cast<c2s_chage_scene*>(buf);
		upadate_scene_state(c_id, packet); //해당 이벤트에 맞게 씬을 업데이트
	}
	break;
	case C2S_ROLL_START:
	{
		c2s_roll_start* packet = reinterpret_cast<c2s_roll_start*>(buf);
		roll_update(packet);
	}
	break;
	case C2S_ROLL_END:
	{
		c2s_roll_end* packet = reinterpret_cast<c2s_roll_end*>(buf);
		int c_id = packet->id;

		clients[c_id].m_pPlayer->UpdateRollEnd(packet);
		for (auto& c : clients)
			if (c.m_pPlayer->GetSceneState() == SCENE_STATE::GAME_SCENE && c.m_id != c_id) //게임씬일때 보내기
				send_roll_end_packet(c.m_id, c_id); //c.secnod.m_id에게 c_id 정보 보내기
	}
	break;
	case C2S_FIRE:
	{
		c2s_fire* packet = reinterpret_cast<c2s_fire*>(buf);
		int c_id = packet->id;

		s2c_fire p;
		p.size = sizeof(s2c_fire);
		p.type = S2C_FIRE;
		p.id = c_id;
		p.pX = packet->pX;
		p.pY = packet->pY;
		p.pZ = packet->pZ;

		p.dX = packet->dX;
		p.dY = packet->dY;
		p.dZ = packet->dZ;

		p.eBulletState = packet->eBulletState;

		for (auto& c : clients)
			if (c.m_pPlayer->GetSceneState() == SCENE_STATE::GAME_SCENE && c.m_id != c_id) //게임씬일때 보내기
				send_packet(c.m_id, &p);
	}
	break;
	case C2C_ZOMBIE_INFO:
	{
		lock_guard<mutex>{clients[c_id].m_cl};
		c2s_zombie_info* packet = reinterpret_cast<c2s_zombie_info*>(buf);
		zombieArr[packet->id].SetPostion({ packet->px,packet->py,packet->pz });
		zombieArr[packet->id].SetState(packet->state);
		for (auto& c : clients)
		{
			if (g_host_id == c.m_id) continue;
			lock_guard<mutex>{c.m_cl};
			if (c.m_pPlayer->GetSceneState() == SCENE_STATE::GAME_SCENE) {
				//npc 이동했다고 알리기
				send_zombieInfo(packet->id, c.m_id);  //c.m_id에게 packet->id좀비 정보 보내기
			}
		}
	}
	break;
	default:
		break;
	}
}
void do_recv(int c_id)
{
	clients[c_id].m_recv_over.m_wsabuf[0].buf = reinterpret_cast<char*>(clients[c_id].m_recv_over.m_buf) + clients[c_id].m_prev_size;
	clients[c_id].m_recv_over.m_wsabuf[0].len = MAX_BUFFER - clients[c_id].m_prev_size;
	memset(&clients[c_id].m_recv_over.m_over, 0, sizeof(clients[c_id].m_recv_over.m_over));
	DWORD rFlag = 0;
	int retval = WSARecv(clients[c_id].m_socket, clients[c_id].m_recv_over.m_wsabuf, 1, NULL, &rFlag, &clients[c_id].m_recv_over.m_over, 0);
	if (retval != NO_ERROR)
	{
		int err_code = WSAGetLastError();
		if (err_code != WSA_IO_PENDING)
			err_display("WSARecv()", err_code);
	}
}


void send_remove_client(int p_id, int other_id)
{
	s2c_remove_client packet;
	packet.size = sizeof(packet);
	packet.type = S2C_REMOVE_CLIENT;
	packet.id = other_id;
	send_packet(p_id, &packet);
}

void disconnect(int key)
{
	lock_guard<mutex> lg{ clients[key].m_cl };
	send_remove_client(key, key);
	closesocket(clients[key].m_socket);
	clients[key].m_state = PLST_INGAE; //다른 클라이언트 접속 막기
	if (clients[key].m_id == g_host_id)
		g_host_id = get_new_hostId();
	for (auto& c : clients) {
		if (c.m_id == key) continue;// 데드락 피하기
		lock_guard<mutex>{c.m_cl};
		if (c.m_state == PLST_CONNECTED && c.m_id != key) {

			if (clients[c.m_id].m_pPlayer->GetSceneState() == SCENE_STATE::GAME_SCENE) {
				//clients[c.second.m_id].m_pPlayer->SetSceneState(SCENE_STATE::GAME_SCENE);
				send_remove_client(c.m_id, key); //c.sencond.m_id에게 key가 종료되었음을 알림
			}
		}

	}
	clients[key].m_state = PLST_FREE;
	/*delete clients[key].m_pPlayer;  //굳이 삭제할 필요 없음
	clients[key].m_pPlayer = nullptr;*/

}
void send_zombieInfo(int zombieID, int c_id)
{

	Vec3 pos = zombieArr[zombieID].GetPostion();
	Vec3 dir = zombieArr[zombieID].GetDir();
	s2c_zombie_info packet;
	packet.type = S2C_ZOMBIE_INFO;
	packet.size = sizeof(s2c_zombie_info);
	packet.px = pos.x;
	packet.py = pos.y;
	packet.pz = pos.z;


	packet.state = zombieArr[zombieID].GetState();
	packet.id = zombieID;


	send_packet(c_id, &packet);

}

//void do_ai()
//{
//	//테스트용으로 일단 좀비 랜덤 무브하기
//	while (true) {
//		for (int i = 0; i < MAX_MONSTER; ++i) {
//			if ((high_resolution_clock::now() - zombieArr[i].getLastTime() > 1s)) {
//				//이동
//				zombieArr[i].move2target();
//				for (auto& p : clients)
//				{
//					if (p.m_pPlayer->GetSceneState() == SCENE_STATE::GAME_SCENE) {
//						//npc 이동했다고 알리기
//						send_zombieInfo(i);
//					}
//				}
//				zombieArr[i].setLastTime(high_resolution_clock::now());
//			}
//
//		}
//	}
//
//}

void worker_thread(HANDLE h_iocp, SOCKET listenSocket)
{
	while (true)
	{

		DWORD numBytes;
		ULONG_PTR ikey;
		WSAOVERLAPPED* over;
		bool b_ret = GetQueuedCompletionStatus(h_iocp, &numBytes, &ikey, &over, INFINITE);//완료검사

		int key = static_cast<int>(ikey);
		if (b_ret == FALSE)
		{
			if (key == SERVER_ID) {//서버오류
				err_display("GetQueuedCompletionStatus()", WSAGetLastError());
				cout << "서버오류\n";
				while (true);
				exit(-1);
			}
			else {
				err_display("GetQueuedCompletionStatus()", WSAGetLastError());
				cout << key << " client 접속종료\n";
				disconnect(key);
			}

		}
		MY_OVER* my_over = reinterpret_cast<MY_OVER*> (over);

		switch (my_over->m_eOP)
		{
		case OP_RECV:
		{
			if (clients[key].m_state == PLST_FREE)  //연결이 끊겨있으면 
				break;
			//패킷 조립, 수행
			unsigned char* packet_ptr = my_over->m_buf;
			int num_data = numBytes + clients[key].m_prev_size; //처리해야할 데이터의 크기=지금받은데이터+지난번에 받은데이터의 나머지

			int packet_size = packet_ptr[0]; //지금 처리해야하는 패킷의 크기



			while (packet_size <= num_data) {
				proccess_packet(key, packet_ptr);
				num_data -= packet_size;
				packet_ptr += packet_size;
				if (0 >= num_data)
					break;
				packet_size = packet_ptr[0];
			}
			clients[key].m_prev_size = num_data;//남은 찌꺼기 보존
			if (0 != num_data)
				memcpy(my_over->m_buf, packet_ptr, sizeof(num_data));//맨앞으로 복사

			do_recv(key);
		}
		break;
		case OP_SEND:
			delete my_over;
			break;
		case OP_ACCEPT:
		{

			int c_id = get_new_id(my_over->c_socket);
			cout << "새로운 ID " << c_id << endl;
			if (c_id != -1) {
#ifdef _DEBUG
				std::cout << "[TCP 서버] " << c_id << "번 클라이언트 접속" << endl;
#endif // _DEBUG

				//clients[c_id] = CLIENT{};

				clients[c_id].m_prev_size = 0;
				clients[c_id].m_recv_over.m_eOP = EOP_TYPE::OP_RECV;

				CreateIoCompletionPort(reinterpret_cast<HANDLE>(clients[c_id].m_socket), h_iocp, c_id, 0);
				do_recv(c_id);
		}
			else
				closesocket(my_over->c_socket);
			//계속 Accept해야됨

			memset(&my_over->m_over, 0, sizeof(my_over->m_over));
			SOCKET c_sock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
			my_over->c_socket = c_sock;
			//AcceptEx()

			AcceptEx(listenSocket, c_sock, my_over->m_buf, SERVER_ID, 32, 32, NULL, &my_over->m_over);
		}
		break;
		default:
			break;
	}



		//do_ai();

}

}
int main()
{

	for (int i = 0; i < MAX_USER + 1; ++i) {
		auto& cl = clients[i];
		cl.m_id = i;
		cl.m_state = PLST_FREE;
		cl.m_pPlayer = new CPlayerObject;
		cl.m_pPlayer->init();
	}
	wcout.imbue(locale("korean"));


	//좀비 정보 초기화
	for (int i = 0; i < MAX_MONSTER; ++i)
	{
		zombieArr[i].init(i);
	}
	cout << "NPC 초기화완료\n";


	int ret = 0;
	WSADATA WSAdata;
	if (WSAStartup(MAKEWORD(2, 2), &WSAdata) != 0)
	{
		cout << "WSAStartUp 실패" << endl;
		exit(-1); //종료시키기
	}

	HANDLE h_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, 0, 0, 0); //iocp핸들

	SOCKET listenSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (listenSocket == SOCKET_ERROR)
	{
		err_display("WSASocket()", WSAGetLastError());
		return 0;
	}
	CreateIoCompletionPort(reinterpret_cast<HANDLE>(listenSocket), h_iocp, 0, 0);//iocp에 소켓등록

	SOCKADDR_IN serverAddr;
	memset(&serverAddr, 0, sizeof(SOCKADDR_IN));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

	ret = ::bind(listenSocket, reinterpret_cast<SOCKADDR*>(&serverAddr), sizeof(serverAddr));
	if (ret == SOCKET_ERROR)
	{
		err_display("bind()", WSAGetLastError());
		return 0;
	}


	ret = listen(listenSocket, SOMAXCONN);
	if (ret == SOCKET_ERROR)
	{
		err_display("listen()", WSAGetLastError());
		return 0;
	}


	SOCKET c_sock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	MY_OVER accept_over; //accept용 overlapped 구조체
	memset(&accept_over.m_over, 0, sizeof(accept_over.m_over));
	accept_over.m_eOP = OP_ACCEPT; //이 overlapped가 무슨용도인지를 나타낸다.
	accept_over.c_socket = c_sock;
	//클라이언트 소켓

	if (c_sock == INVALID_SOCKET) {
		err_display("WSASocket()", WSAGetLastError());
		return 0;
	}

	BOOL b_ret = AcceptEx(listenSocket, c_sock, accept_over.m_buf, SERVER_ID, 32, 32, NULL, &accept_over.m_over);
	if (b_ret == FALSE)
	{
		int error = WSAGetLastError();
		if (error != WSA_IO_PENDING) {
			err_display("AcceptEx()", WSAGetLastError());
			while (true);
		}
	}
	vector<thread> worker_threads;
	for (int i = 0; i < 5; ++i) worker_threads.emplace_back(worker_thread, h_iocp, listenSocket);//thread 4개

	for (auto& t : worker_threads) t.join();


	/*thread ai_thread{ do_ai };
	ai_thread.join();*/
	closesocket(listenSocket);
	WSACleanup();



}
