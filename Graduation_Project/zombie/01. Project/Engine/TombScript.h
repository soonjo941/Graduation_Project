#pragma once
#include "Script.h"

class CTombScript :
	public CScript
{
private:
	float hp = 100;
	bool IsDisappear = false;
	bool IsExplore = false;
	int Id;
	CSound* ExplosionSound;

public:
	virtual void update();

	virtual void OnCollisionEnter(CCollider2D* _pOther);
	virtual void OnCollisionExit(CCollider2D* _pOther);
	virtual void OnCollision(CCollider2D* _pOther);

	bool CheckIsDisappear() { return IsDisappear; };
	bool GetIsExplosion() { return IsExplore; };
	void SetIsExplosion(bool b) { IsExplore = b; };
	CSound* GetExplosionSound() { return ExplosionSound; };

public:
	CLONE(CTombScript);

	float getHp() { return hp; };
	void setHp(float n) { hp = n; };
	float getId() { return Id; };
	void setId(float n) { Id = n; };

public:
	CTombScript();
	virtual ~CTombScript();
};

