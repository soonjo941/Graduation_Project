#include "stdafx.h"
#include "RoarShotScript.h"
#include "MonsterScript.h"

CRoarShotScript::CRoarShotScript()
	: CScript((UINT)SCRIPT_TYPE::ROARSHOTSCRIPT)
{
	this->SetName(L"RoarShotScript");
}


CRoarShotScript::~CRoarShotScript()
{
}

void CRoarShotScript::update()
{
	if (BulletActive)
	{

		Vec3 vPos = Transform()->GetLocalPos();
		//vPos.y += 400.f * DT;
		vPos += vBulletDir * BulletSpeed * DT;
		Transform()->SetLocalPos(vPos);

		float range = sqrt((vBulletStartPos.x - vPos.x) * (vBulletStartPos.x - vPos.x) + (vBulletStartPos.z - vPos.z) * (vBulletStartPos.z - vPos.z));

		if (vPos.x >= 5300 || vPos.x <= -5300 || vPos.z >= 5300 || vPos.z <= -5300 || range > 700)
			BulletActive = false;

	}
	else
	{
		Transform()->SetLocalPos(Vec3(-20000.f, -20000.f, -20000.f));	
	}
}

void CRoarShotScript::OnCollisionEnter(CCollider2D* _pOther)
{
	if (BulletActive)
	{
		wstring s = _pOther->GetObj()->GetName();
		if (L"Player Object" == _pOther->GetObj()->GetName())
		{
			//DeleteObject(GetObj());
			BulletActive = false;
			_pOther->GetObj()->GetScript<CPlayerScript>()->getDamage(damage);
		}
	}
}

void CRoarShotScript::OnCollision(CCollider2D* _pOther)
{
	if (BulletActive)
	{
		wstring s = _pOther->GetObj()->GetName();
		if (L"Player Object" == _pOther->GetObj()->GetName())
		{
			//DeleteObject(GetObj());
			BulletActive = false;
		}
	}
}